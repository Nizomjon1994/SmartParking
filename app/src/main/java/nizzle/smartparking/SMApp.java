package nizzle.smartparking;

import android.app.Application;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import dagger.ObjectGraph;
import nizzle.smartparking.module.MainModule;

/**
 * Created by Hajiev Nizomjon on 29.06.2016.
 */

public class SMApp extends Application {

    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        objectGraph = ObjectGraph.create(new MainModule(this));
        inject(this);

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);
        super.onCreate();
    }

    public void inject(Object object) {
        try {
            objectGraph.inject(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
