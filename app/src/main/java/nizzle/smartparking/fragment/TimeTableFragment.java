package nizzle.smartparking.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import nizzle.smartparking.R;
import nizzle.smartparking.model.CarData;
import nizzle.smartparking.presenter.DurationPresenter;
import nizzle.smartparking.util.CustomDialog;
import nizzle.smartparking.view.DurationView;
import rx.Observable;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 27.08.2016.
 */
public class TimeTableFragment extends MvpFragment<DurationPresenter> implements DurationView {


    //    @BindView(R.id.date_picker_enter_btn)
//    FloatingActionButton enterBtn;
    @BindView(R.id.date_picker_exit_btn)
    FloatingActionButton exitBtn;
    @BindView(R.id.table_layout)
    TableLayout mTabLayout;
    @BindView(R.id.enter_time_spinner)
    TextView mEnterSpinner;
    @BindView(R.id.exit_time_spinner)
    TextView mExitSpinner;
//    @BindColor(R.color.white)
//    int whiteColor;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    private int mYear, mMonth, mDay, mHour, mMinute;
    StringBuilder enterBuilder, exitBuilder;
    private boolean isEnterTime = false;
    private boolean isExitTime = false;


    @Override
    protected int getLayout() {
        return R.layout.fragment_time_table;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Calendar c = Calendar.getInstance();

        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        enterBuilder = new StringBuilder();
        exitBuilder = new StringBuilder();


    }

    @OnClick(R.id.enter_time_spinner)
    void enterSpinner() {
        if (!mEnterSpinner.getText().toString().equals("")) {
            enterBuilder.setLength(0);
        }
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        enterBuilder.append(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        final Calendar c = Calendar.getInstance();
                        mHour = c.get(Calendar.HOUR_OF_DAY);
                        mMinute = c.get(Calendar.MINUTE);
                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {

                                    @Override
                                    public void onTimeSet(TimePicker view, int hourOfDay,
                                                          int minute) {
                                        enterBuilder.append(" ");
                                        enterBuilder.append(hourOfDay + ":" + minute);
                                        isEnterTime = true;
                                        updateEnterTime(enterBuilder.toString());
                                    }
                                }, mHour, mMinute, false);
                        timePickerDialog.show();
                    }

                }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

    @OnClick(R.id.exit_time_spinner)
    void exitSpinner() {
        if (!mExitSpinner.getText().toString().equals("")) {
            exitBuilder.setLength(0);
        }
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        exitBuilder.append(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        final Calendar c = Calendar.getInstance();
                        mHour = c.get(Calendar.HOUR_OF_DAY);
                        mMinute = c.get(Calendar.MINUTE);
                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                                new TimePickerDialog.OnTimeSetListener() {

                                    @Override
                                    public void onTimeSet(TimePicker view, int hourOfDay,
                                                          int minute) {
                                        exitBuilder.append(" ");
                                        exitBuilder.append(hourOfDay + ":" + minute);
                                        updateExitTime(exitBuilder.toString());
                                    }
                                }, mHour, mMinute, false);
                        timePickerDialog.show();
                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }


    @OnClick(R.id.date_picker_exit_btn)
    void toSendBackend() {
        presenter.getCarDetails();
    }


    @Override
    public void showListToTable(List<CarData> carDates) {

        final float scale = getResources().getDisplayMetrics().density;
        int padding_10dp = (int) (5 * scale + 0.5f);
        int padding_20dp = (int) (20 * scale + 0.2f);

        for (int i = 0; i < carDates.size(); i++) {
            final CarData carData = carDates.get(i);
            final TableRow tableRow = new TableRow(getActivity());
            tableRow.setPadding(0, padding_20dp, 0, padding_20dp);
            tableRow.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                    , ViewGroup.LayoutParams.WRAP_CONTENT));

            TextView username = new TextView(getActivity());
            username.setPadding(padding_10dp, 0, 0, 0);
            username.setTextColor(getResources().getColor(R.color.white));
            username.setText(carData.getUserName() == null ? "" : carData.getUserName());
            username.setBackgroundResource(R.drawable.table_cell_bg);
            tableRow.addView(username);

            TextView enterTime = new TextView(getActivity());
            enterTime.setText(carData.getEnterTime() == null ? "" : stringToMineFormat(carData.getEnterTime()));
            enterTime.setTextColor(getResources().getColor(R.color.white));
            enterTime.setBackgroundResource(R.drawable.table_cell_bg);
            enterTime.setPadding(padding_10dp, 0, 0, 0);
            tableRow.addView(enterTime);

            TextView exitTime = new TextView(getActivity());
            exitTime.setText(carData.getExitTime() == null ? "" : stringToMineFormat(carData.getExitTime()));
            exitTime.setBackgroundResource(R.drawable.table_cell_bg);
            exitTime.setTextColor(getResources().getColor(R.color.white));
            exitTime.setPadding(padding_10dp, 0, 0, 0);
            tableRow.addView(exitTime);

            TextView total_calculation = new TextView(getActivity());
            total_calculation.setText(carData.getTottalyTimeCalculation() == null ? "" : carData.getTottalyTimeCalculation());
            total_calculation.setPadding(padding_10dp, 0, 0, 0);
            total_calculation.setTextColor(getResources().getColor(R.color.white));
            tableRow.addView(total_calculation);

            tableRow.setId(i);
            tableRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CustomDialog cdd = new CustomDialog(getContext(), carData);
                    cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    cdd.show();
                    Window window = cdd.getWindow();
                    window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                    Toast.makeText(getActivity(), "id : " + tableRow.getId(), Toast.LENGTH_SHORT).show();
                }
            });
            mTabLayout.addView(tableRow, new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
        }
    }

    @Override
    public Observable<List<CarData>> getCarDetails() {

        return api.getCarListByDuration(enterBuilder.toString(), exitBuilder.toString());
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void setError(String error) {
        Toast.makeText(getActivity(), "Error : " + error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNoNetwork() {
        Toast.makeText(getActivity(), "No internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected DurationPresenter createPresenter() {
        return new DurationPresenter(this);
    }

    private String stringToMineFormat(String fromServer) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat mineDateFormat = new SimpleDateFormat("MM-dd/hh:mm");

        try {
            return mineDateFormat.format(simpleDateFormat.parse(fromServer));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void updateEnterTime(String test) {
        mEnterSpinner.setText(test);
    }

    private void updateExitTime(String test) {
        mExitSpinner.setText(test);
    }
}
