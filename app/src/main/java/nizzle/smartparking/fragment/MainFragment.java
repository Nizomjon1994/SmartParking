package nizzle.smartparking.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.ramotion.foldingcell.FoldingCell;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;
import nizzle.smartparking.R;
import nizzle.smartparking.adapter.CarAdapterDatabinding;
import nizzle.smartparking.adapter.TestAdapter;
import nizzle.smartparking.event.CarEvent;
import nizzle.smartparking.model.Car;
import nizzle.smartparking.model.CarData;
import nizzle.smartparking.presenter.MainPresenter;
import nizzle.smartparking.view.MainView;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 27.08.2016.
 */
public class MainFragment extends MvpFragment<MainPresenter> implements MainView {

    @BindView(R.id.mainListView)
    ListView mListView;
    @BindView(R.id.webView)
    WebView webview;
    @BindView(R.id.camera_view)
    FrameLayout cameraView;

    CarAdapterDatabinding mCarAdapter;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        webview.setWebViewClient(new WebViewClient());
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
        webview.setWebChromeClient(new WebChromeClient());
        onEvent();
        presenter.getCarList();
    }

    private void onEvent() {
        bus.toObservable().subscribe(new Action1<Object>() {
            @Override
            public void call(Object event) {
                if (event instanceof CarEvent) {
                    if (((CarEvent) event).getType() == CarEvent.Type.LIST_CAR) {
                        final TestAdapter adapter = new TestAdapter(getActivity(), ((CarEvent) event).getCars(), bus);
                        adapter.setDefaultRequestBtnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(getActivity(), "DEFAULT HANDLER FOR ALL BUTTONS", Toast.LENGTH_SHORT).show();
                            }
                        });
                        mListView.setAdapter(adapter);
                        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                ((FoldingCell) view).toggle(false);
                                adapter.registerToggle(pos);
                            }
                        });
                    } else if (((CarEvent) event).getType() == CarEvent.Type.SINGLE_CAR) {
                        cameraView.setVisibility(View.VISIBLE);
                        initWebView(((CarEvent) event).getCar().getCameraIpAddress());
                    } else if ((((CarEvent) event).getType()) == CarEvent.Type.CALLING) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + ((CarEvent) event).getCar().getPhoneNumber()));
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        getActivity().startActivity(callIntent);
                    }
                }
            }
        });
    }

    @Optional
    @OnClick(R.id.close_btn)
    void closeView() {
        cameraView.setVisibility(View.GONE);
    }


    private void initWebView(String url) {

        webview.loadUrl(url);
    }



    @Override
    protected MainPresenter createPresenter() {
        return new MainPresenter(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_main;
    }



    @Override
    public void showCarList(final List<CarData> cars) {

        Observable.from(cars)
                .filter(new Func1<CarData, Boolean>() {
                    @Override
                    public Boolean call(CarData carData) {
                        return carData.getCarNumber()!=null;
                    }
                })
                .toList()
                .subscribe(new Action1<List<CarData>>() {
                    @Override
                    public void call(List<CarData> carDatas) {
                        final TestAdapter adapter = new TestAdapter(getActivity(), carDatas,bus);
                        adapter.setDefaultRequestBtnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(getActivity(), "DEFAULT HANDLER FOR ALL BUTTONS", Toast.LENGTH_SHORT).show();
                            }
                        });
                        mListView.setAdapter(adapter);
                        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                ((FoldingCell) view).toggle(false);
                                adapter.registerToggle(pos);
                            }
                        });

                    }
                });
    }

    @Override
    public void showCarDetails(Car car) {

    }

    @Override
    public void showCamera() {

    }

    @Override
    public Observable<List<CarData>> getCars() {
        return api.getCars();
    }



    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setError(String error) {
        Toast.makeText(getActivity(), "Error : " + error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNoNetwork() {
        Toast.makeText(getActivity(), R.string.no_connection, Toast.LENGTH_SHORT).show();

    }

}
