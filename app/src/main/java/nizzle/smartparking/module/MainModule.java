package nizzle.smartparking.module;


import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import nizzle.smartparking.SMApp;
import nizzle.smartparking.activities.BaseActivity;
import nizzle.smartparking.activities.LoginActivity;
import nizzle.smartparking.activities.MainActivity;
import nizzle.smartparking.activities.SearchActivity;
import nizzle.smartparking.activities.SignUpActivity;
import nizzle.smartparking.activities.VideoPlayerActivity;
import nizzle.smartparking.api.Api;
import nizzle.smartparking.fragment.BaseFragment;
import nizzle.smartparking.fragment.MainFragment;
import nizzle.smartparking.fragment.TimeTableFragment;
import nizzle.smartparking.navigator.Navigator;
import nizzle.smartparking.presenter.BasePresenter;
import nizzle.smartparking.util.C;
import nizzle.smartparking.util.RxBus;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Hajiev Nizomjon on 29.06.2016.
 */

@Module(
        library = true,
        injects = {
                SMApp.class,
                BaseActivity.class,
                LoginActivity.class,
                SignUpActivity.class,
                MainActivity.class,
                SearchActivity.class,
//                CarDetailActivity.class,
                VideoPlayerActivity.class,
                BaseFragment.class,
                MainFragment.class,
                TimeTableFragment.class

        }
)

public class MainModule {


    private SMApp smApp;

    public MainModule(SMApp smApp) {
        this.smApp = smApp;
    }


    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }


    @Provides
    @Singleton
    RestAdapter provideRetrofit() {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader("Accept", "application/json");
            }
        };
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(1, TimeUnit.MINUTES);
        client.setReadTimeout(1, TimeUnit.MINUTES);
        OkClient okClient = new OkClient(client);

        return new RestAdapter.Builder()
                .setEndpoint(C.BASE_URL)
                .setClient(okClient)
                .setRequestInterceptor(requestInterceptor)
                .build();
    }


    @Provides
    @Singleton
    Api provideApi(RestAdapter restAdapter) {
        return restAdapter.create(Api.class);
    }


    @Provides
    @Singleton
    Navigator provideNavigator() {
        return new Navigator();
    }

    @Provides
    @Singleton
    RxBus provideRxBus() {
        return new RxBus();
    }

    @Provides
    @Singleton
    BasePresenter provideBasePresenter() {
        return new BasePresenter();

    }
}


