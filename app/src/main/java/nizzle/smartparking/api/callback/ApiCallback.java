package nizzle.smartparking.api.callback;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 27.08.2016.
 */
public interface ApiCallback<T> {

    void onSuccess(T model);

    void onFailure(int code , String msg);

    void onCompleted();

    void onNetworkError();

}
