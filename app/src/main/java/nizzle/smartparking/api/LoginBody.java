package nizzle.smartparking.api;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 06.08.2016.
 */
public class LoginBody {

    String email;
    String password;
    int checked;

    public LoginBody(String email, String password, int checked) {
        this.email = email;
        this.password = password;
        this.checked = checked;
    }
}
