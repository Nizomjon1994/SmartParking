package nizzle.smartparking.api;


import java.util.List;

import nizzle.smartparking.api.callback.BaseCallback;
import nizzle.smartparking.api.results.SignUpResult;
import nizzle.smartparking.model.CarData;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;


/**
 * Created by Hajiev Nizomjon on 29.06.2016.
 */

public interface Api {

    @FormUrlEncoded
    @POST("/signup.php/")
    void signup(@Field("username") String username, @Field("email") String email, @Field("password") String password, BaseCallback<SignUpResult> callback);

    @FormUrlEncoded
    @POST("/login.php/")
    void login(@Field("username") String username, @Field("password") String password, BaseCallback<SignUpResult> callback);

    @FormUrlEncoded
    @POST("/login.php/")
    Observable<SignUpResult> login(@Field("customer_name") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("/signup.php/")
    Observable<SignUpResult> register(@Field("customer_name") String username,
                                      @Field("password") String password,
                                      @Field("customer_phone_number") String phonenum,
                                      @Field("car_number") int carNumber,
                                      @Field("customer_img") String carimg);

    @GET("/search.php/")
    Observable<List<CarData>> search(@Query("q") String username);


    @GET("/camera/cam1.php/")
    Observable<List<CarData>> getCarList();
    @GET("/cars.php/")
    Observable<List<CarData>> getCars();


    @GET("/timepicker.php/")
    Observable<List<CarData>> getCarListByDuration(@Query("from") String from, @Query("to") String to);
}
