package nizzle.smartparking.navigator;

import android.content.Context;
import android.content.Intent;

import javax.inject.Inject;
import javax.inject.Singleton;

import nizzle.smartparking.activities.LoginActivity;
import nizzle.smartparking.activities.MainActivity;
import nizzle.smartparking.activities.SignUpActivity;

/**
 * Created by Hajiev Nizomjon on 12.07.2016.
 */

@Singleton
public class Navigator {

    @Inject
    public Navigator() {
    }

    public void navigateSignUpScreen(Context context) {
        if (context != null) {
            Intent intent = new Intent(context, SignUpActivity.class);
            context.startActivity(intent);
        }

    }

    public void navigateLoginScreen(Context context) {
        if (context != null) {
            Intent intent = new Intent(context, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        }
    }

    public void navigateMainScreen(Context context) {
        if (context != null) {
            Intent intent = new Intent(context, MainActivity.class);
            context.startActivity(intent);
        }
    }

//    public void navigateCarDetailsScreen(Context context, String carDetails) {
//        if (context != null) {
//            Intent intent = new Intent(context, CarDetailActivity.class);
//            intent.putExtra("car_body", carDetails);
//            context.startActivity(intent);
//        }
//    }

}
