package nizzle.smartparking.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 08.08.2016.
 */
public class BitmapUtils {


    public static Bitmap decode(Uri uri, int minWidth, int minHeight) {

        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(new File(uri.getPath())), null, o);

            int imgWidth = o.outWidth;
            int imgHeight = o.outHeight;

            int orientation = getExifOrientation(uri.getPath());

            //rotated bitmaps;
            if (orientation >= 5 && orientation <= 8) {
                imgWidth = o.outHeight;
                imgHeight = o.outWidth;
            }

            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (imgWidth / scale / 2 >= minWidth && imgHeight / scale / 2 >= minHeight) {
                scale *= 2;
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bm = BitmapFactory.decodeStream(new FileInputStream(new File(uri.getPath())), null, o2);
            bm = resolveOrientationFromExif_(orientation, bm);
            return bm;
        } catch (Throwable t) {

        }
        return null;
    }

    public static Bitmap decode(String path, int reqWidth, int reqHeight) {

        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(new File(path)), null, o);

            int imgWidth = o.outWidth;
            int imgHeight = o.outHeight;

            int orientation = getExifOrientation(path);

            //rotated bitmaps;
            if (orientation >= 5 && orientation <= 8) {
                imgWidth = o.outHeight;
                imgHeight = o.outWidth;
            }

            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (imgWidth / scale / 2 >= reqWidth && imgHeight / scale / 2 >= reqHeight) {
                scale *= 2;
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bm = BitmapFactory.decodeStream(new FileInputStream(new File(path)), null, o2);

            bm = resolveOrientationFromExif_(orientation, bm);

            return bm;
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return null;
    }

    public static boolean resizeIfRequired(String inputPath, String outputPath, int maxWidth, int maxHeight) {

        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(new File(inputPath)), null, o);

            int imgWidth = o.outWidth;
            int imgHeight = o.outHeight;

            int orientation = getExifOrientation(inputPath);

            //rotated bitmaps;
            if (orientation >= 5 && orientation <= 8) {
                imgWidth = o.outHeight;
                imgHeight = o.outWidth;
            }

            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (imgWidth / scale > maxWidth || imgHeight / scale > maxHeight) {
                scale *= 2;
            }

            if (scale > 1) {
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                Bitmap bm = BitmapFactory.decodeStream(new FileInputStream(new File(inputPath)), null, o2);
                bm = resolveOrientationFromExif_(orientation, bm);
                File outFile = new File(outputPath);
                FileOutputStream out = new FileOutputStream(outFile);
                bm.compress(Bitmap.CompressFormat.JPEG, 85, out);
                bm.recycle();
                out.flush();
                out.close();
                return true;
            } else {
                return false;
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return false;
    }

    public static void exifInvalidate(String imagePath) {
        try {
            int orientation = getExifOrientation(imagePath);
            if (isOrientationRotated(orientation)) {
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                InputStream is = new FileInputStream(new File(imagePath));
                Bitmap bm = BitmapFactory.decodeStream(is, null, o2);
                try {
                    is.close();
                } catch (IOException e) {
                }

                bm = resolveOrientationFromExif(orientation, bm);

                File outFile = new File(imagePath);
                FileOutputStream out = new FileOutputStream(outFile);
                bm.compress(Bitmap.CompressFormat.JPEG, 85, out);
                bm.recycle();
                out.flush();
                out.close();
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static boolean resizeAndExifInvalidate(String inputPath, String outputPath, int maxWidth, int maxHeight) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(new File(inputPath)), null, o);

            int imgWidth = o.outWidth;
            int imgHeight = o.outHeight;

            int orientation = getExifOrientation(inputPath);

            //rotated bitmaps;
            if (isOrientationRotated(orientation)) {
                imgWidth = o.outHeight;
                imgHeight = o.outWidth;
            }

            float heightScale = (float) imgHeight / maxHeight;
            float widthScale = (float) imgWidth / maxWidth;

            if (heightScale > 1f || widthScale > 1f) {
                float scale = Math.max(heightScale, widthScale);
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                // optimized decoding bitmap;
                o2.inSampleSize = (int) scale;
                Bitmap bm = BitmapFactory.decodeStream(new FileInputStream(new File(inputPath)), null, o2);
                imgWidth = bm.getWidth();
                imgHeight = bm.getHeight();
                if (isOrientationRotated(orientation)) {
                    imgWidth = bm.getHeight();
                    imgHeight = bm.getWidth();
                }

                heightScale = (float) imgHeight / maxHeight;
                widthScale = (float) imgWidth / maxWidth;

                Bitmap bmap;
                if (heightScale > 1f || widthScale > 1f) {
                    scale = Math.max(widthScale, heightScale);
                    int width = (int) (bm.getWidth() / scale);
                    int height = (int) (bm.getHeight() / scale);
                    bmap = Bitmap.createScaledBitmap(bm, width, height, false);
                    bm.recycle();
                } else {
                    bmap = bm;
                }

                boolean saveExifOrientation = false;
                if (isOrientationRotated(orientation)) {
                    try {
                        bmap = resolveOrientationFromExif(orientation, bmap);
                    } catch (Throwable throwable) {
                        saveExifOrientation = true;
                    }
                }

                File outFile = new File(outputPath);
                FileOutputStream out = new FileOutputStream(outFile);
                bmap.compress(Bitmap.CompressFormat.JPEG, 85, out);
                bmap.recycle();
                out.flush();
                out.close();

                if (saveExifOrientation) {
                    try {
                        ExifInterface exif = new ExifInterface(outputPath);
                        exif.setAttribute(ExifInterface.TAG_ORIENTATION, String.valueOf(orientation));
                        exif.saveAttributes();
                    } catch (Throwable t) {
                        return false;
                    }
                }
                return true;
            } else if (isOrientationRotated(orientation)) {
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                Bitmap bm = BitmapFactory.decodeStream(new FileInputStream(new File(inputPath)), null, o2);
                try {
                    bm = resolveOrientationFromExif(orientation, bm);
                } catch (Throwable throwable) {
                    bm.recycle();
                    return false;
                }
                File outFile = new File(outputPath);
                FileOutputStream out = new FileOutputStream(outFile);
                bm.compress(Bitmap.CompressFormat.JPEG, 85, out);
                bm.recycle();
                out.flush();
                out.close();
                return true;
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return false;
    }

    private static boolean isOrientationRotated(int orientation) {
        return orientation == ExifInterface.ORIENTATION_TRANSPOSE
                || orientation == ExifInterface.ORIENTATION_ROTATE_90
                || orientation == ExifInterface.ORIENTATION_TRANSVERSE
                || orientation == ExifInterface.ORIENTATION_ROTATE_270;
    }

    private static boolean isOrientationNormal(int orientation) {
        return orientation == ExifInterface.ORIENTATION_UNDEFINED
                || orientation == ExifInterface.ORIENTATION_NORMAL;
    }

    @Deprecated
    public static Bitmap resolveOrientationFromExif_(int orientation, Bitmap bitmap) {

        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }

        try {
            Bitmap oriented = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return oriented;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return bitmap;
        }
    }

    private static Bitmap resolveOrientationFromExif(int orientation, Bitmap bitmap) throws Throwable {
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }

        Bitmap oriented = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        bitmap.recycle();
        return oriented;
    }

    private static int getExifOrientation(String src) {
        int orientation = 1;

        try {
            ExifInterface exif = new ExifInterface(src);
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        } catch (Throwable t) {
        }
        return orientation;
    }


}
