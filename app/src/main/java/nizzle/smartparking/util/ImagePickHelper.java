package nizzle.smartparking.util;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 08.08.2016.
 */
public class ImagePickHelper {

    private Activity activity;
    private Uri outputFileUri;
    private String outputFilePath;
    private Fragment fragment;
    private String mCurrentPhotoPath;
    private String chooserTitle;
    private int requestCode;
    private String pickedImageFilePath;
    private Uri mPickedImageUri;
    private boolean mAccessDenied;

    public ImagePickHelper(Activity activity, int requestCode, String chooserTitle) {
        this.activity = activity;
        this.requestCode = requestCode;
        this.chooserTitle = chooserTitle;
    }

    public ImagePickHelper(Fragment fragment, int requestCode, String chooserTitle) {
        this(fragment.getActivity(), requestCode, chooserTitle);
        this.fragment = fragment;
    }

    public ImagePickHelper(Activity activity, Bundle savedInstanceState) {
        this.activity = activity;
        this.requestCode = savedInstanceState.getInt("REQUEST_CODE");
        this.outputFilePath = savedInstanceState.getString("OUTPUT_FILE_PATH");
        this.mPickedImageUri = savedInstanceState.getParcelable("PICKED_MEDIA_URI");
        this.mAccessDenied = savedInstanceState.getBoolean("ACCESS_DENIED");
    }

    public ImagePickHelper(Fragment fragment, Bundle savedInstanceState) {
        this.fragment = fragment;
        this.requestCode = savedInstanceState.getInt("REQUEST_CODE");
        this.outputFilePath = savedInstanceState.getString("OUTPUT_FILE_PATH");
        this.mPickedImageUri = savedInstanceState.getParcelable("PICKED_MEDIA_URI");
        this.mAccessDenied = savedInstanceState.getBoolean("ACCESS_DENIED");
    }

    public void pick() {
        mAccessDenied = false;
        pickedImageFilePath = null;
        mPickedImageUri = null;
        final File tempImageFile = createImageFile();
        if (tempImageFile == null) {
            // TODO open gallery only
            return;
        }

        outputFileUri = Uri.fromFile(tempImageFile);
        outputFilePath = tempImageFile.getAbsolutePath();
//        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = activity.getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, chooserTitle);

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
//
        if (fragment != null) {
            fragment.startActivityForResult(chooserIntent, requestCode);
        } else {
            activity.startActivityForResult(chooserIntent, requestCode);
        }
    }

    private File createImageFile() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "temp";//"JPEG_" + timeStamp + "_";
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = new File(storageDir, "camera.jpg");
            mCurrentPhotoPath = image.getAbsolutePath();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return image;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (this.requestCode == requestCode) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    final boolean isCamera;
                    if (data == null || data.getData() == null) {
                        isCamera = true;
                    } else {
                        final String action = data.getAction();
                        if (action == null) {
                            isCamera = false;
                        } else {
                            isCamera = action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
                        }
                    }

//                    int reqSize = getResources().getDimensionPixelSize(R.dimen.report_image_thumb_max_size);
                    if (isCamera) {
                        onImageReady(outputFilePath);
                        //                        showAttachment(imagePath);
                        //                        if (data != null && data.hasExtra("data")) {
                        //                            Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
                        //                            image.setImageBitmap(imageBitmap);
                        //                            return;
                        //                        }
                        //                        image.setImageBitmap(BitmapUtils.decode(outputFilePath, reqSize, reqSize));
                    } else {
                        mPickedImageUri = data.getData();
                        onImageReady(FileUtils.getPath(activity, mPickedImageUri));
                    }
                } catch (SecurityException se) {
                    mAccessDenied = true;
                } catch (Exception e) {

                }
            }
        }
    }

    public boolean isAccessDenied() {
        return mAccessDenied;
    }

    public void accessGranted() {
        if (mPickedImageUri != null) {
            onImageReady(FileUtils.getPath(activity, mPickedImageUri));
        }
    }

    private void onImageReady(String filePath) {
        pickedImageFilePath = filePath;
    }

    public String getPickedImageFilePath() {
        return pickedImageFilePath;
    }

    public Bitmap getPicketImageBitmap(int width, int height) {
        if (pickedImageFilePath != null) {
            return BitmapUtils.decode(pickedImageFilePath, width, height);
        }

        return null;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putString("OUTPUT_FILE_PATH", outputFilePath);
        outState.putInt("REQUEST_CODE", requestCode);
        outState.putParcelable("PICKED_MEDIA_URI", mPickedImageUri);
        outState.putBoolean("ACCESS_DENIED", mAccessDenied);
    }

    public int getRequestCode() {
        return requestCode;
    }
}

