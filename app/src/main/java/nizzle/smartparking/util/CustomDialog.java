package nizzle.smartparking.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import nizzle.smartparking.R;
import nizzle.smartparking.activities.VideoPlayerActivity;
import nizzle.smartparking.databinding.CustomDialogLayoutBinding;
import nizzle.smartparking.model.CarData;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 06.09.2016.
 */
public class CustomDialog extends Dialog implements View.OnClickListener {

    public Activity c;
    public Dialog d;
    @BindView(R.id.btn_ok)
    Button yes;
    @BindView(R.id.ip_cam_btn)
    Button ipBtnCamera;
    @BindView(R.id.user_phone)
    TextView phoneNumeber;
    private Context mContext;

    public CarData mCarData;
    CustomDialogLayoutBinding binding;

    public CustomDialog(Activity a, CarData carData) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.mCarData = carData;
    }

    public CustomDialog(Context context, CarData carData) {
        super(context);
        mCarData = carData;
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.custom_dialog_layout, null, false);
        setContentView(binding.getRoot());
        ButterKnife.bind(this);
        binding.setCarData(mCarData);
        yes.setOnClickListener(this);
        ipBtnCamera.setOnClickListener(this);
        phoneNumeber.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ip_cam_btn:
                if (mCarData.getCameraIpAddress() != null) {
                    Intent intent = new Intent(mContext, VideoPlayerActivity.class);
                    intent.putExtra("URL", mCarData.getCameraIpAddress());
                    mContext.startActivity(intent);
                }
                break;
            case R.id.btn_ok:
                dismiss();
                break;

            default:
                break;
        }
        dismiss();
    }
}
