package nizzle.smartparking.eventhandlers;

import android.support.design.widget.Snackbar;
import android.view.View;

import nizzle.smartparking.model.CarData;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 12.08.2016.
 */
public class ClickHandlers {

    private final CarData mCarData;

    public ClickHandlers(CarData carData) {
        mCarData = carData;
    }




    void onExpandViewClick(View view) {
        Snackbar.make(view, "Test", Snackbar.LENGTH_LONG).show();
    }

}
