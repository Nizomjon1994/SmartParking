package nizzle.smartparking.presenter;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 04.08.2016.
 */
public interface Presenter<V> {

    void attachView(V view);

    void detachView();

    void onCreate();

    void onResume();

    void onPause();

    void onDestroy();

    void onStart();

    void onStop();
}
