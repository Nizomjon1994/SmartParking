package nizzle.smartparking.presenter;

import nizzle.smartparking.api.results.SignUpResult;
import nizzle.smartparking.view.LoginView;
import rx.Observer;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 04.08.2016.
 */
public class LoginPresenter extends BasePresenter implements Observer<SignUpResult> {

    private LoginView mLoginView;

    public LoginPresenter(LoginView loginView) {
        mLoginView = loginView;
    }

    public void login() {
        unSubscribeAll();
        mLoginView.showProgress();
        subscribe(mLoginView.postLogin(), LoginPresenter.this);
    }

    public boolean validate(String email, String password, String username) {
        boolean valid = true;

        if (email.isEmpty()) {
            mLoginView.setError("enter a valid username");
            valid = false;
        } else {
            mLoginView.setError(null);
        }

        if (username != null && username.isEmpty()) {
            mLoginView.setError("enter a valid username");
            valid = false;
        } else {
            mLoginView.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            mLoginView.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            mLoginView.setError(null);
        }
        return valid;
    }


    @Override
    public void onNext(SignUpResult signUpResult) {
        mLoginView.onLogin(signUpResult);
    }

    @Override
    public void onError(Throwable e) {
        mLoginView.hideProgress();
        mLoginView.setError(e.getMessage());
    }

    @Override
    public void onCompleted() {
        mLoginView.hideProgress();
    }
}
