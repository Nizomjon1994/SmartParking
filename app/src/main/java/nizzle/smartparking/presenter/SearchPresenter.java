package nizzle.smartparking.presenter;

import java.util.List;

import nizzle.smartparking.api.callback.ApiCallback;
import nizzle.smartparking.api.callback.SubscriberCallback;
import nizzle.smartparking.model.CarData;
import nizzle.smartparking.view.SearchView;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 11.08.2016.
 */
public class SearchPresenter extends BasePresenter<SearchView> {


    public SearchPresenter(SearchView searchView) {
        attachView(searchView);
    }

    public void search(String keyword) {
        mvpView.hideNoResult();
        mvpView.showProgress();
        unSubscribeAll();
        subscribe(mvpView.getCarDetails(keyword), new SubscriberCallback<>(new ApiCallback<List<CarData>>() {
            @Override
            public void onSuccess(List<CarData> model) {
                mvpView.onSearchResult(model);
            }

            @Override
            public void onFailure(int code, String msg) {
                mvpView.setError(msg);

            }

            @Override
            public void onCompleted() {
                mvpView.hideProgress();
                mvpView.finishActivity();
            }

            @Override
            public void onNetworkError() {
                mvpView.hideProgress();
                mvpView.showNoNetwork();
            }
        }));
    }

}
