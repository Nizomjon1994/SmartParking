package nizzle.smartparking.presenter;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 04.08.2016.
 */
@Singleton
public class BasePresenter<V> implements Presenter<V> {

    public V mvpView;

    private CompositeSubscription mCompositeSubscription;

    @Inject
    public BasePresenter() {
    }

    @Override
    public void attachView(V view) {
        this.mvpView = view;
    }

    @Override
    public void detachView() {
        this.mvpView = null;
        unSubscribeAll();

    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onResume() {
        configureSubscription();
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {

        unSubscribeAll();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    private CompositeSubscription configureSubscription() {
        if (mCompositeSubscription == null || mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription = new CompositeSubscription();
        }
        return mCompositeSubscription;
    }
    protected void unSubscribeAll() {
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription.clear();
        }
    }

    protected <T> void subscribe(Observable<T> observable , Observer<T> observer){
        Subscription subscriptions = observable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.computation())
                .subscribe(observer);
        configureSubscription().add(subscriptions);
    }

}
