package nizzle.smartparking.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ramotion.foldingcell.FoldingCell;

import java.util.HashSet;
import java.util.List;

import butterknife.ButterKnife;
import nizzle.smartparking.BR;
import nizzle.smartparking.R;
import nizzle.smartparking.model.CarData;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 12.08.2016.
 */
public class CarAdapterDatabinding extends RecyclerView.Adapter<CarAdapterDatabinding.ItemViewHolder> {

    private List<CarData> mCars;
    private boolean hasCamara;
    private OnItemClickListener mOnItemClickListener;
    private View.OnClickListener defaultRequestBtnClickListener;
    private HashSet<Integer> unfoldedIndexes = new HashSet<>();

    public CarAdapterDatabinding(List<CarData> cars, boolean hasCamara, OnItemClickListener mOnItemClickListener) {
        mCars = cars;
        this.hasCamara = hasCamara;
        this.mOnItemClickListener = mOnItemClickListener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater vi = LayoutInflater.from(parent.getContext());
        View view = vi.inflate(R.layout.cell, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        CarData carData = mCars.get(position);
        if (!hasCamara) {
            carData.setHasCameraIcon(false);
        }
        holder.getDataBinding().setVariable(BR.carData, carData);
        holder.getDataBinding().executePendingBindings();

        if (unfoldedIndexes.contains(position)) {
            holder.cel.unfold(true);
        } else {
            holder.cel.fold(true);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.cel.toggle(false);
                registerToggle(position);
            }
        });

    }

    public void registerToggle(int position) {
        if (unfoldedIndexes.contains(position))
            registerFold(position);
        else
            registerUnfold(position);
    }

    public void registerFold(int position) {
        unfoldedIndexes.remove(position);
    }

    public void registerUnfold(int position) {
        unfoldedIndexes.add(position);
    }

    @Override
    public int getItemCount() {
        return mCars.size();
    }

    public View.OnClickListener getDefaultRequestBtnClickListener() {
        return defaultRequestBtnClickListener;
    }

    public void setDefaultRequestBtnClickListener(View.OnClickListener defaultRequestBtnClickListener) {
        this.defaultRequestBtnClickListener = defaultRequestBtnClickListener;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {


        FoldingCell cel;

//        @BindView(R.id.title_price)
//        Button btn;

        private View mView;

        ViewDataBinding mDataBinding;


        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mView = itemView;
            cel = (FoldingCell) mView;
            mDataBinding = DataBindingUtil.bind(cel);

        }


        public ViewDataBinding getDataBinding() {
            return mDataBinding;
        }

    }

    public interface OnItemClickListener {
        void cameraClick(CarData audio, int position, View view);

    }

}
