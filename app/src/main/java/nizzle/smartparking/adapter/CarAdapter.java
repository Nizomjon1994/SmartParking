package nizzle.smartparking.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nizzle.smartparking.R;
import nizzle.smartparking.model.Car;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 11.08.2016.
 */
public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {


    private List<Car> mCars;
    private Context mContext;
    private onClickListener mOnClickListener;

    public CarAdapter(List<Car> cars, Context context, onClickListener onClickListener) {
        mCars = cars;
        mContext = context;
        mOnClickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.car_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Car car = mCars.get(position);
        holder.mCar = car;

        holder.userName.setText(car.getUserName());
        holder.carColor.setText(car.getCarColor());

    }

    @Override
    public int getItemCount() {
        return mCars.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_name)
        TextView userName;
        @BindView(R.id.car_color)
        TextView carColor;

        private Car mCar;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.car_container)
        void click() {
            if (mOnClickListener != null && mCar != null)
                mOnClickListener.click(mCar);
        }
    }

    public interface onClickListener {

        void click(Car car);
    }
}
