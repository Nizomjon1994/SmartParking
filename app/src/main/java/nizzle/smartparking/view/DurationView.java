package nizzle.smartparking.view;

import java.util.List;

import nizzle.smartparking.model.CarData;
import rx.Observable;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 05.09.2016.
 */
public interface DurationView extends BaseView {

    void showListToTable(List<CarData> carDates);

    Observable<List<CarData>> getCarDetails();

}
