package nizzle.smartparking.view;


import nizzle.smartparking.api.results.SignUpResult;
import rx.Observable;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 04.08.2016.
 */
public interface LoginView extends BaseView {


    void navigateToHome();

    void onLogin(SignUpResult result);

    Observable<SignUpResult> postLogin();

}
