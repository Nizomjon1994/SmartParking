package nizzle.smartparking.view;

import java.util.List;

import nizzle.smartparking.model.CarData;
import rx.Observable;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 03.09.2016.
 */
public interface CameraView extends BaseView {

    void showCamera();

    void showErrorCam();

    Observable<List<CarData>> getCarDetails();

    void showCarList(List<CarData> carList);
}
