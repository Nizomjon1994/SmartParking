package nizzle.smartparking.activities;

import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ramotion.foldingcell.FoldingCell;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import nizzle.smartparking.R;
import nizzle.smartparking.adapter.CarAdapterDatabinding;
import nizzle.smartparking.adapter.TestAdapter;
import nizzle.smartparking.model.CarData;
import nizzle.smartparking.presenter.CameraPresenter;
import nizzle.smartparking.util.systemuivis.SystemUiHelper;
import nizzle.smartparking.view.CameraView;
import rx.Observable;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 11.08.2016.
 */
public class VideoPlayerActivity extends MvpActivity<CameraPresenter> implements CameraView {


    @BindView(R.id.video_player)
    WebView webview;

    //    @BindView(R.id.recycler_view)
//    RecyclerView mRecyclerView;
    @BindView(R.id.mainListView)
    ListView mListView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private MediaController mediaController;
    private SystemUiHelper systemUiHelper;
    private AudioManager mAudioManager;
    String url;
    CarAdapterDatabinding mCarAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);


        setContentView(R.layout.activity_video_player);
        setHomeAsUp();

//        String title = getIntent().getStringExtra("TITLE");
        url = getIntent().getStringExtra("URL");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
//            getSupportActionBar().setTitle(title);
        }

        Uri uri = Uri.parse(url);
        showCamera();
//        presenter.getCarList();


    }

    @Override
    protected CameraPresenter createPresenter() {
        return new CameraPresenter(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add(Menu.NONE, R.id.menu_rotate, Menu.FIRST, R.string.rotate)
                .setIcon(R.drawable.ic_screen_rotation);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void showCamera() {

        mediaController = new MediaController(this);
        mediaController.setAnchorView(webview);
        webview.setWebViewClient(new WebViewClient());
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
        webview.setWebChromeClient(new WebChromeClient());
        webview.loadUrl(url);

    }

    @Override
    public void showErrorCam() {

    }

    @Override
    public Observable<List<CarData>> getCarDetails() {
        return api.getCarList()
                .delay(100, TimeUnit.MILLISECONDS);
    }

    @Override
    public void showCarList(List<CarData> carList) {



        final TestAdapter adapter = new TestAdapter(VideoPlayerActivity.this, carList,bus);

        adapter.setDefaultRequestBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(VideoPlayerActivity.this, "DEFAULT HANDLER FOR ALL BUTTONS", Toast.LENGTH_SHORT).show();
            }
        });

        mListView.setAdapter(adapter);

        // set on click event listener to list view
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                // toggle clicked cell state
                ((FoldingCell) view).toggle(false);
                // register in adapter that state for selected cell is toggled
                adapter.registerToggle(pos);
            }
        });
    }


    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setError(String error) {
        Toast.makeText(VideoPlayerActivity.this, error, Toast.LENGTH_SHORT).show();
    }

}
