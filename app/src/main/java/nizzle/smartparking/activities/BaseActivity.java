package nizzle.smartparking.activities;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import nizzle.smartparking.R;
import nizzle.smartparking.SMApp;
import nizzle.smartparking.api.Api;
import nizzle.smartparking.navigator.Navigator;
import nizzle.smartparking.presenter.BasePresenter;
import nizzle.smartparking.util.RxBus;

/**
 * Created by Hajiev Nizomjon on 29.06.2016.
 */

public class BaseActivity extends AppCompatActivity {

    public static final int REQUEST_PICK_PROFILE_IMAGE = 105;


    @Inject
    Api api;


    @Inject
    Navigator navigator;

    @Inject
    BasePresenter mBasePresenter;

    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    Gson mGson;

    @Inject
    RxBus bus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((SMApp) getApplication()).inject(this);
        mBasePresenter.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBasePresenter.onResume();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void setHomeAsUp() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }


    public void showNoNetwork() {
        if (!isFinishing()) {
            Toast.makeText(this, R.string.no_connection, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBasePresenter.onDestroy();
    }

    private void inject() {
        ((SMApp) getApplication()).inject(this);
    }
}
